async function syncRecognizeModelSelection(
  uri,
  model,
  sampleRateHertz,
  languageCode
) {
  // [START speech_transcribe_model_selection]
  // Imports the Google Cloud client library for Beta API
  /**
   * TODO(developer): Update client library import to use new
   * version of API when desired features become available
   */
  const speech = require("@google-cloud/speech").v1p1beta1;
  const fs = require("fs");

  // Creates a client
  const client = new speech.SpeechClient();

  /**
   * TODO(developer): Uncomment the following lines before running the sample.
   */
  //   const uri = uri;
  //   const model = model;
  //   const sampleRateHertz = sampleRateHertz;
  //   const languageCode = languageCode;

  const config = {
    // encoding: encoding,
    sampleRateHertz: sampleRateHertz,
    languageCode: languageCode,
    model: model,
    enableSpeakerDiarization: true,
    diarizationSpeakerCount: 2
  };
  const audio = {
    uri: uri
  };

  const request = {
    config: config,
    audio: audio
  };

  const [operation] = await client.longRunningRecognize(request);
  // Get a Promise representation of the final result of the job
  const [response] = await operation.promise();
  const transcription = response.results
    .map(result => result.alternatives[0].transcript)
    .join("\n");
  console.log(`Transcription: ${transcription}`);
  console.log(`Speaker Diarization:`);
  const result = response.results[response.results.length - 1];
  const wordsInfo = result.alternatives[0].words;
  // Note: The transcript within each result is separate and sequential per result.
  // However, the words list within an alternative includes all the words
  // from all the results thus far. Thus, to get all the words with speaker
  // tags, you only have to take the words list from the last result:
  wordsInfo.forEach(a =>
    console.log(` word: ${a.word}, speakerTag: ${a.speakerTag}`)
  );
  // [END speech_transcribe_model_selection]
}

syncRecognizeModelSelection(
  "gs://gao-transcripts-audio/gao_pod.flac",
  "default",
  44100,
  "en-US"
).catch(console.error);
